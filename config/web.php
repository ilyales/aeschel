<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$webroot = dirname(__DIR__) . '/web';
$web = rtrim(dirname($_SERVER["SCRIPT_NAME"]), '/');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute'=> 'main/page',
    'layout' => 'base',
    'language' => 'ru-RU',
     // Path aliases
    'aliases' => [
        '@uploadPath' => $webroot . '/uploads', // @webroot isn't available yet (@webroot/uploads).
        '@uploadUrl' => $web . '/uploads', // @web isn't available yet (@web/uploads).
        
    ], 
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'sdfhsdw54yherbxs4',
            'baseUrl'=> '',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['main/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
       'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'login'=>'main/login',
                '/<view>' => 'main/page',
                'napravleniya/<catagoryView:>/<subCatagoryView:>' =>  'main/napravleniya/',
                'napravleniya/<catagoryView:>' => 'main/napravleniya/',                   
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],

        'thumbnail' => [
            'class' => 'sadovojav\image\Thumbnail',
            'cachePath' => 'thumb',
        ],

        'i18n' => [
            'translations' => [
                'ycm' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'ycm'       => 'ycm.php',
                ],
            ],
        ],
    ],        
    ],
     

    // Modules
    'modules' => [
        'ycm' => [
            'class' => 'janisto\ycm\Module',
            'admins' => ['admin'], // admin usernames array
            'urlPrefix' => 'admin', // url for ycm
            'registerModels' => [
                /**
                 * Add models.
                 *
                 * Usage:
                 * [
                 *   'name' => 'class definition',
                 * ]
                 *
                 * name:
                 * It's used as a url slug and by default the folder name for uploads. You can override the upload
                 * folder name in class configuration array: 'folderName' => 'xxx'
                 *
                 * class definition:
                 * - a string: representing the class name of the object to be created
                 * - a configuration array: the array must contain a `class` element which is treated as the object class,
                 *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
                 * - a PHP callable: either an anonymous function or an array representing a class method (`[$class or $object, $method]`).
                 *   The callable should return a new instance of the object being created.
                 */
                'portfolio' => 'app\models\Portfolio',
                'product' => 'app\models\Product',
                'product_subcat' => 'app\models\ProductSubcat',
                'product_manuf' => 'app\models\ProductManuf',
                // 'example'  => 'app\models\Example',
                // 'basic2' => [
                //     'class' => 'app\models\BasicSearch',
                //     'folderName' => 'basic', // Use the same path for uploads
                //     //'public' => 1, // Set 1 by default
                // ],
                // 'example' => 'app\models\Example',
                // 'post' => 'app\models\Post',
                // 'category' => 'app\models\Category',
                // 'blog' => 'app\models\Blog',
                // 'department' => 'app\models\Department',
                // 'common' => 'app\models\CommonAdmin',
            ],
            'registerControllers' => [
                /*
                 * Add controllers.
                 *
                 * Mapping from controller ID to controller configurations.
                 * Each name-value pair specifies the configuration of a single controller.
                 * A controller configuration can be either a string or an array.
                 * If the former, the string should be the fully qualified class name of the controller.
                 * If the latter, the array must contain a 'class' element which specifies
                 * the controller's fully qualified class name, and the rest of the name-value pairs
                 * in the array are used to initialize the corresponding controller properties. For example,
                 *
                 * [
                 *   'account' => 'app\controllers\UserController',
                 *   'article' => [
                 *      'class' => 'app\controllers\PostController',
                 *      'defaultAction' => 'xxx',
                 *   ],
                 * ]
                 */
                 
                'test' => [
                    'class' => 'app\controllers\admin\TestController',
                    'defaultAction' => 'index', // override default value
                ],
            ],
            'registerUrlRules' => [
                /**
                 * Add URL rules.
                 *
                 * Usage:
                 * [
                 *   'pattern' => 'route',
                 *   'pattern' => 'route',
                 * ]
                 */
                'test/<action:\w+>' => 'test/<action>',
                'test' => 'test/index',
            ],
            'sidebarItems' => [
                /**
                 * Add Nav widget items.
                 *
                 * A list of items in the nav widget. Each array element represents a single
                 * menu item which can be either a string or an array with the following structure:
                 *
                 * - label: string, required, the nav item label.
                 * - url: optional, the item's URL. Defaults to "#".
                 * - visible: boolean, optional, whether this menu item is visible. Defaults to true.
                 * - linkOptions: array, optional, the HTML attributes of the item's link.
                 * - options: array, optional, the HTML attributes of the item container (LI).
                 * - active: boolean, optional, whether the item should be on active state or not.
                 * - items: array|string, optional, the configuration array for creating a [[Dropdown]] widget,
                 *   or a string representing the dropdown menu. Note that Bootstrap does not support sub-dropdown menus.
                 *
                 * If a menu item is a string, it will be rendered directly without HTML encoding.
                 */
                // ['label' => 'Test index', 'url' => ['test/index']],
                // ['label' => 'Test view', 'url' => ['test/view']],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
