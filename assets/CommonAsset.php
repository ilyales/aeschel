<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class CommonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/build.css',
        'css/slick.css',
    ];
    public $js = [
        'js/dist/libs.js',
        YII_ENV_DEV ? 'js/dist/common-debug.js' : 'js/dist/common.js'
    ];
    public $depends = [
        
    ];
}
