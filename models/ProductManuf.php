<?php

namespace app\models;

use Yii;
use janisto\ycm\behaviors\FileBehavior;

/**
 * This is the model class for table "product_manuf".
 *
 * @property int $id
 * @property string $name
 * @property string $img
 */
class ProductManuf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => FileBehavior::className(),
                'folderName' => 'product_manuf',
            ]
        ];
    }

    public $adminNames = ['Производители оборудования'];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_manuf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['img'], 'image', 'maxSize' => 1024 * 1024 * 1, 'maxWidth' => 2560, 'maxHeight' => 2560],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название произоводителя',
            'img' => 'Логотип',
        ];
    }

    /**
     * Config for attribute widgets (ycm)
     *
     * @return array
     */
    public function attributeWidgets()
    {
        /**
         * You should configure attribute widget as: ['field', 'widget'].
         *
         * If you need to pass additional configuration to the widget, add them as: ['field', 'widget', 'key' => 'value'].
         * The first two items are sliced from the array, and then it's passed to the widget as: ['key' => 'value'].
         *
         * There are couple of exceptions to this slicing:
         * If you use 'hint', 'hintOptions', 'label' or 'labelOptions' the widget will render hint and/or label and
         * then it will unset those keys and pass the array to the widget.
         *
         * If you use 'widget', you must also set 'widgetClass'.
         *
         * Check each widget to see what options you can override/set.
         */

        return [
            ['img', 'image'],
        ];
    }

    public static function getImgUrl($imgName) {
        return "/web/uploads/product_manuf/img/".$imgName;
    }

    /**
     * Grid view columns for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#column-classes
     * @see http://www.yiiframework.com/doc-2.0/guide-output-formatter.html#other-formatters
     * @see http://www.bsourcecode.com/yiiframework2/gridview-in-yiiframework-2-0/#GridView-Column-Content-Options
     */
    public function gridViewColumns()
    {
        return [
            name,
            [
                'attribute' => 'img',
                'format' => ['image', ['height' => '20']],
                'value' => function($model) {
                    return $model->getFileUrl('img');
                },
                'contentOptions' => ['class' => 'image-class'],
            ],
        ];
    }

    /**
     * Grid view sort for ActiveDataProvider (ycm)
     *
     * @return array
     *
     * @see http://www.yiiframework.com/doc-2.0/guide-output-data-widgets.html#sorting-data
     */
    public function gridViewSort()
    {
        return [
            'defaultOrder' => [
                'name' => SORT_ASC,
            ]
        ];
    }
}
