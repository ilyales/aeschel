<?php

namespace app\models;

use Yii;

class ProductRootcat {

    const rootCats = [
        '0' => [
            'name'=>'Измерительное',
            'viewName' => 'izmeritelnoe-oborudivanie',
        ],
        '1' => [
            'name'=>'Инженерное',
            'viewName' => 'injenernoe-oborudivanie',
        ],
        '2' => [
            'name'=>'Автоматизация',
            'viewName' => 'oborudivanie-dly-avtomatizacii',
        ],
    ];

    public static function getIdByViewName($viewName) {
        foreach (self::rootCats as $id=>$rootCat) {
            if ($rootCat['viewName']==$viewName) {
                $rootCatId = $id;
                break;
            }
        }
        return $rootCatId;
    }

    public static function getNames() {
        $rootCatsNames = [];
        foreach (self::rootCats as $id=>$rootCat) {
            $rootCatsNames[$id] = $rootCat['name'];
        }
        return $rootCatsNames;
    }
}