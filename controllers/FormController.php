<?php

namespace app\controllers;

use yii\web\Controller;
use app\components\Mailer;
use app\components\AjaxResponse;

class FormController extends Controller
{
    public $enableCsrfValidation = false;

	public function actionSend() {
        $mailer = new Mailer;
        $response = new AjaxResponse;
        try {
            $mailer->sendContacts($_POST);
        }
        catch (Exception $e) {
            Yii::log($e->getMessage(),'error', 'mail');
            $response->setError($e->getMessage());
        }
        $response->send();
    }
}