<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Url;

use app\models\LoginForm;
use app\models\Portfolio;
use app\models\ProductManuf;
use app\models\ProductRootcat;
use app\components\ProductsManager;

class MainController extends Controller
{
    public $defaultAction = 'page';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionPage($view='index')
    {
        if ($view=='index') {
            $portfolios = Portfolio::find()->asArray()->all();
            for ($i=0;$i<count($portfolios);$i++) {
                $photoPath = Yii::getAlias('@uploadPath')."/portfolio/photo/".$portfolios[$i]['photo'];
                $photoParamsMini =  [
                    'thumbnail' => [
                        'width' => 320,
                        'height' => 213,
                    ],
                    'placeholder' => [
                        'width' => 320,
                        'height' => 213
                    ]
                ];
                $photoParamsBig =  [
                    'thumbnail' => [
                        'width' => 450,
                        'height' => 300,
                    ],
                    'placeholder' => [
                        'width' => 450,
                        'height' => 300
                    ]
                ];
                $portfolios[$i]['photo-mini'] = '/web/'.Yii::$app->thumbnail->url($photoPath, $photoParamsMini);
                $portfolios[$i]['photo-big'] = '/web/'.Yii::$app->thumbnail->url($photoPath, $photoParamsBig);
            }
            return $this->render($view,['portfolios'=>$portfolios]);
        }

        return $this->render($view);
    }

    public function actionNapravleniya($catagoryView="",$subCatagoryView="")
    {   
        if ($catagoryView!="komplectaciya") {
            $view = 'napravleniya/'.$catagoryView;
            $this->layout = "napravleniyaLayout"; 
        }
        else {
            if ($subCatagoryView!=null) {


            $view = 'napravleniya/komplectaciya-category';
            $this->layout = "komplectaciyaLayout";

            $productRootcatId = ProductRootcat::getIdByViewName($subCatagoryView);
            $productSubcats = ProductsManager::getSubcatsWithProducts($productRootcatId); 

            switch ($subCatagoryView) {
                case 'izmeritelnoe-oborudivanie':
                    $this->view->params['komplectaciya-category-title'] = "Измерительное оборудование";
                    break;  
                case 'injenernoe-oborudivanie':
                    $this->view->params['komplectaciya-category-title'] = "Инженерное обрудование";
                    break;
                case 'oborudivanie-dly-avtomatizacii':
                    $this->view->params['komplectaciya-category-title'] = "Обрудование для автоматизации";
                    break;              
            }

            $this->view->params['subCatagoryView'] = $subCatagoryView;
            $this->view->params['productSubcats'] = $productSubcats;           
            }
            else {
                $allProductSubcats = [];
                $allProductSubcats[0] = ProductsManager::getSubcats(0); 
                $allProductSubcats[1] = ProductsManager::getSubcats(1);
                $allProductSubcats[2] = ProductsManager::getSubcats(2);

                $this->view->params['allProductSubcats'] = $allProductSubcats;
                $this->layout = "napravleniyaLayout"; 
                $view = 'napravleniya/komplectaciya';
            }
        }
        $this->view->params['catagoryView'] = $catagoryView; 
        return $this->render($view);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['admin']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return Yii::$app->response->redirect(['admin/model/list','name'=>'portfolio']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}