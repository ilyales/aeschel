<?php

namespace app\components;

use app\models\ProductSubcat;
use app\models\ProductManuf;
use app\models\Product;

class ProductsManager {

    //возвращает только те подкатегории, в которых есть товары
    public static function getSubcats($productRootcatId) {
        $subcats = ProductSubcat::find()
            ->innerJoin('product','product.subcat_id=product_subcat.id')
            ->where('root_catagory = :root_catagory_id', [':root_catagory_id' => $productRootcatId])
            ->asArray()
            ->all();                    
        return $subcats;
    }

    public static function getSubcatsWithProducts($productRootcatId) {
        
        $rows = Product::find()
            ->select('product.*, product_subcat.id as subcatagory_id,product_subcat.name as subcatagory_name,product_manuf.id as manufactory_id ,product_manuf.name as manufactory_name,product_manuf.img as manufactory_img')
            ->leftJoin('product_subcat', 'product_subcat.id = product.subcat_id')
            ->leftJoin('product_manuf', 'product_manuf.id = product.manuf_id')
            ->where('product_subcat.root_catagory = :root_catagory_id', [':root_catagory_id' => $productRootcatId])
            ->orderBy(['subcatagory_id'=>SORT_ASC, 'manufactory_id'=>SORT_ASC])
            ->asArray()
            ->all();

        $subcats = [];
        $lastSubcatId = -1;
        $lastManufId = -1;

        foreach ($rows as $row) {
            if ($row['subcatagory_id']!=$lastSubcatId) {
                
                $subcats[] = [
                    'id' => $row['subcatagory_id'],
                    'name' => $row['subcatagory_name'],
                    'manufactories' => [
                        [
                            'name' => $row['manufactory_name'],
                            'img' => ProductManuf::getImgUrl($row['manufactory_img']),
                            'products' => [
                                [
                                    'name' => $row['name'],
                                    'img'  => Product::getImgUrl($row['img'])
                                ]
                            ]
                        ]
                    ],
                ]; 
                $lastSubcatId = $row['subcatagory_id'];
                $lastManufId = $row['manufactory_id'];
            }
            else {                
                $lastSubcatIndex = count($subcats) - 1;
                if ($row['manufactory_id']!=$lastManufId) {
                    $subcats[$lastSubcatIndex]['manufactories'][] = [
                        'name' => $row['manufactory_name'],
                        'img' => ProductManuf::getImgUrl($row['manufactory_img']),
                        'products' => [
                            [
                                'name' => $row['name'],
                                'img'  => Product::getImgUrl($row['img'])
                            ]
                        ]
                    ];
                    $lastManufId = $row['manufactory_id'];
                }
                else {
                    $lastManufIndex = count($subcats[$lastSubcatIndex]['manufactories']) - 1;
                    $subcats[$lastSubcatIndex]['manufactories'][$lastManufIndex]['products'][] = [
                        'name' => $row['name'],
                        'img'  => Product::getImgUrl($row['img'])
                    ];
                }                
            }
        }

        return $subcats;
    }   
}