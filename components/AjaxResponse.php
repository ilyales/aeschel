<?php

namespace app\components;


class AjaxResponse {

	public $status;
	public $data;

	function __construct() {
    	$this->data = array();
    	$this->status = true;
   	}

	public function setDataItem($itemName,$itemVal)
	{
		$this->data[$itemName] = $itemVal;
	}

	public function setDataItems($items)
	{
		$this->data = $items;
	}

	public function setError($message)
	{
		$this->status = false;
		$this->data['message'] = $message;
	}

	public function send()
	{
		$response = array('status'=>$this->status);

		foreach ($this->data as $key=>$value)
		{
			$response[$key] = $value;
		}
		
		echo json_encode($response);
	}
}