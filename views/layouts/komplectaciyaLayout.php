<?php 
use app\assets\EquipmentsAsset;
EquipmentsAsset::register($this); 

?>

<?php $this->beginContent('@app/views/layouts/napravleniyaLayout.php'); ?>

<div class="text-page_title text-page_title__komplect">КОМПЛЕКТАЦИЯ ОБОРУДОВАНИЕМ</div>

<?php if (YII_ENV_DEV): ?>
	<div class="text-page_text">
		<?php echo $content; ?>
	</div>
	<?php echo $this->render('@app/views/main/napravleniya/components/komplectaciya-order-popup',[]); ?>
<?php else: ?>
	<div class="text-page_catalog-load">
		Каталог обрудования обновляется.
	</div>
<?php endif; ?>
		
<?php $this->endContent(); ?>