<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\assets\CommonAsset;
use yii\helpers\Url;

CommonAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=1300">
    <title>АЭС</title>
   
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="header-top-wr">
        <div class="header-top p-line">
            <a href="tel:+73517313210" class="header-top_phone">+7 (351) 731-32-10</a>
            <a href="mailto:mail@aeschel.ru" class="header-top_email">mail@aeschel.ru</a>
            <div class="header-top_call-btn" onclick="window.orderPopup.show()">ОСТАВИТЬ ЗАЯВКУ</div>
        </div>
    </div>

    <div class="header-wrap">
        <img class="heade_bg1" src="/img/soti-mini.png">
        <img class="heade_bg2" src="/img/soti-mini.png">
        <div class="header p-line">
            <div class="header_logo">
                <a href="/">
                    <img src="/img/logo-header.png" class="header_logo-img">
                </a>
            </div>  

            <div class="header_menu-wr">
                <ul class="header_menu-ul">
                    <li class="header_menu-li">
                        <a href="/#about" class="header_menu-a">О КОМПАНИИ</a>
                    </li>
                    <li class="header_menu-li">
                        <a href="/#works" class="header_menu-a">ВЫПОЛНЕННЫЕ ПРОЕКТЫ</a>
                    </li>
                    <!-- <li class="header_menu-li">
                        <a href="#" class="header_menu-a">ЛИЦЕНЗИИ</a>
                    </li> -->
                    <li class="header_menu-li">
                        <a href="<?php echo Url::toRoute(['main/page', 'view' => 'contacti']); ?>" class="header_menu-a">КОНТАКТЫ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <?= $content ?>

    <div class="main-form">
        <div class="p-line">
            <form class="main-form_form">
                <div class="main-form_title">Напишите нам</div>
                <div class="main-form_subtitle">и мы обязательно с Вами свяжемся</div> 
                <div class="main-form_inputs-line">
                    <div class="main-form_inp-wrap formWrapName">
                        <input class="main-form_inp-val formInput" type="text" placeholder="Ваше имя">
                        <div class="main-form_inp-req">обязательное поле</div>
                    </div>
                    <div class="main-form_inp-wrap formWrapPhone">
                        <input class="main-form_inp-val formInput" type="text" placeholder="Ваш телефон">
                        <div class="main-form_inp-req">обязательное поле</div>
                    </div>
                    <div class="main-form_inp-wrap formWrapEmail">
                        <input class="main-form_inp-val formInput" type="email" placeholder="Ваш e-mail">
                        <div class="main-form_inp-req">обязательное поле</div>
                    </div>
                </div>
                <div class="main-form_text-wrap formWrapText">
                    <textarea class="main-form_text-val formInput" placeholder="Сообщение"></textarea>
                    <div class="main-form_inp-req">обязательное поле</div>
                </div>
                
                <div class="main-form_bottom formWrapConfirm">
                    <div class="main-form_confirmCehck-wrap ">
                        <input type="checkbox" id="main-form_confirmCehck" class="main-form_confirmCehck formInput">
                        <label for="main-form_confirmCehck" class="main-form_confirmCehck-label">Даю согласие на обработку <a href="#" class="main-form_confirmCehck-link  formShowFullConfirm">пересональных данных</a></label>
                        <div class="main-form_inp-req">вы не дали согласия на обработку персональных данных</div>
                    </div>
                    <div class="main-form_full-confirm formFullConfirm">
                        <div class="main-form_full-confirm-close formCloseFullConfirm"></div>
                        <input type="checkbox" id="main-form_full-confirm-check" class="main-form_full-confirm-check formFullConfirmCheck">
                        <label for="main-form_full-confirm-check" class="main-form_full-confirm-label">Я выражаю свое согласие на осуществление обработки (сбора, систематизации, накопления, хранения, уточнения, обновления, изменения, использования, обезличивания, блокирования и уничтожения) в том числе автоматизированной, моих персональных данных, указанных на сайте, в соответствии с требованиями Федерального закона от 27.07.2006 года № 152-ФЗ «О персональных данных».</label>
                    </div>
                    <div class="main-form_send-btn formSendBtn">отправить</div>
                </div>
            </form>
            <div class="main-form_sending">
                <img src="/img/loading.gif" class="main-form_sending-img">
            </div>
            <div class="main-form_success">
                <div class="main-form_success-title">Сообщение отправлено!</div>
                <div class="main-form_success-text">Мы свяжемся с вами в ближайшее время.</div>
            </div>
            <div class="main-form_fail">
                <div class="main-form_fail-title">Произошла ошибка. Сообщение не доставлено.</div>
                <div class="main-form_fail-text">В случае повторения ошибки, свяжитесь с нами по телефону, указанному в контактах.</div>
            </div>  
        </div>
    </div>

    <div class="footer p-line">
        <div class="footer_clm1">
            <a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'proektirovanie']); ?>" class="f-menu_title">НАПРАВЛЕНИЯ</a>
            <a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'proektirovanie']); ?>" class="f-menu_item">Проектирование</a>
            <a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya']); ?>" class="f-menu_item">Комплектация оборудованием</a>
            <a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'stroy-montaj']); ?>" class="f-menu_item">Строительно-монтажные, пусконаладочные работы</a>
            <a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'garantiynoe-i-servisnoe']); ?>" class="f-menu_item">Гарантийное и сервисное обслуживани</a>
        </div>
        <div class="footer_clm2">
            <a href="#works" class="f-menu_title">ВЫПОЛНЕННЫЕ РАБОТЫ</a>
            <!--  <a href="#" class="f-menu_title">ЛИЦЕНЗИИ</a> -->
            <a href="<?php echo Url::toRoute(['main/page', 'view' => 'contacti']); ?>" class="f-menu_title">КОНТАКТЫ</a>
            <p class="footer_copyright">© «Агентство энергетического сервиса»</p>
            <p class="footer_design">
                Дизайн сайта
                <a class="footer_design-a">Студия Евгении Гайда</a>
            </p>
        </div>
        <div class="footer_clm3">
            <img class="footer_logo" src="/img/logo-footer.png">
            <a href="tel:+73517313210" class="footer_phone">
                <span class="footer_phone-pre">+7 (351)</span>
                731-32-10
            </a>
            <a href="mailto:mail@aeschel.ru" class="footer_email">mail@aeschel.ru</a>
            <div class="footer_call-btn" onclick="window.orderPopup.show()">ЗАКАЗАТЬ ЗВОНОК</div>
            
        </div>
    </div>

    <!--всплывающее окно заказать звононк -->
    <?php echo $this->render('@app/views/layouts/components/order-popup',[]); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
