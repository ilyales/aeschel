<div class="order-popup">
	<div class="order-popup__close" onclick="window.orderPopup.close()"></div>
	<div class="order-popup__inputs">
		<div class="order-popup__inp order-popup__inp_title">
			<div class="order-popup__title">Напишите нам</div>
			<div class="order-popup__title-info">и мы свяжемся с вами в ближайшее время</div>
		</div>
		<div class="order-popup__inp formWrapName">
			<div class="order-popup__inp-label">Имя <span class="order-popup__inp-req formError">обязательное поле</span></div>
			<input type="text" class="order-popup__inp-val formInput">
		</div>
		<div class="order-popup__inp formWrapPhone">
			<div class="order-popup__inp-label">Телефон <span class="order-popup__inp-req formError">обязательное поле</span></div>
			<input type="text" class="order-popup__inp-val formInput">
		</div>
		<div class="order-popup__inp formWrapEmail">
			<div class="order-popup__inp-label">Email <span class="order-popup__inp-req formError">обязательное поле</span></div>
			<input type="text" class="order-popup__inp-val formInput">
		</div>
		<div class="order-popup__inp formWrapText">
			<div class="order-popup__inp-label">Сообщение <span class="order-popup__inp-req formError">обязательное поле</span></div>
			<textarea type="text" class="order-popup__text-val formInput"></textarea>
		</div>
		<div class="order-popup__inp order-popup__inp_action formWrapConfirm">
			<div class="order-popup__confirmCehck-wrap ">
				<input type="checkbox" id="order-popup__confirmCehck" class="order-popup__confirmCehck formInput">
				<label for="order-popup__confirmCehck" class="order-popup__confirmCehck-label">Даю согласие на обработку <a href="/" class="order-popup__confirmCehck-link formShowFullConfirm">пересональных данных</a></label>
			</div>
			<div class="order-popup__full-confirm formFullConfirm">
				<div class="order-popup__full-confirm-close formCloseFullConfirm"></div>
				<input type="checkbox" id="order-popup__full-confirm-check" class="order-popup__full-confirm-check formFullConfirmCheck">
				<label for="order-popup__full-confirm-check" class="order-popup__full-confirm-label">Я выражаю свое согласие на осуществление обработки (сбора, систематизации, накопления, хранения, уточнения, обновления, изменения, использования, обезличивания, блокирования и уничтожения) в том числе автоматизированной, моих персональных данных, указанных на сайте, в соответствии с требованиями Федерального закона от 27.07.2006 года № 152-ФЗ «О персональных данных».</label>
			</div>
			<div class="order-popup__sendBtn formSendBtn">Отправить</div>
			<div class="order-popup__inp-req order-popup__inp-req_confirm">Необходимо дать согласие на обработку персональных данных</div>
		</div>
	</div>
	<div class="order-popup__sending">
		<img src="/img/loading.gif" class="order-popup__sending-img">
	</div>
	<div class="order-popup__success">
		<div class="order-popup__success-text">Сообщение отправлено!</div>
		<div class="order-popup__closeBtn" onclick="window.orderPopup.close()">Закрыть</div>
	</div>
	<div class="order-popup__fail">
		<div class="order-popup__fail-text">Произошла ошибка. Сообщение не доставлено.</div>
		<div class="order-popup__closeBtn" onclick="window.orderPopup.close()">Закрыть</div>
	</div>		
</div>
<div class="order-popup__bg" onclick="window.orderPopup.close()"></div>