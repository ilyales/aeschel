<?php
use app\assets\MainAsset;
use yii\helpers\Url;

MainAsset::register($this);
?>

<div class="main-slider">
	<img class="main-slider_bg" src="/img/main-slider/slide1.jpg">
</div>


<div class="products">
	<img class="products_bg" src="/img/plusi-bg.png">
	<div class="products-line ">	
		<div class="label-line p-line">
			<img class="label-line_img" src="/img/green-label.png">
		</div>
		<div class="title products_title p-line">
			Основные направления деятельности			
		</div>
	
		<!-- <div class="products_items products_items-1">
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'proektirovanie']); ?>" class="products_item products_item-1">
				<img src="/img/aes1.png" class="products_item-img">
				<span class="products_item-title products_item-title__single">Проектирование</span>
				<div class="products_item-hover"></div>
			</a>
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'garantiynoe-i-servisnoe']); ?>" class="products_item products_item-2">
				<img src="/img/aes2.png" class="products_item-img">
				<span class="products_item-title">Гарантийное и сервисное<br> обслуживание</span>
				
				<div class="products_item-hover"></div>
			</a>
			
		</div>

		<div class="products_items products_items-2">
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'stroy-montaj']); ?>" class="products_item products_item-1">
				<img src="/img/aes3.png" class="products_item-img">
				<span class="products_item-title">Строительно-монтажные,<br>пусконаладочные работы</span>
				<div class="products_item-hover"></div>
			</a>
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya']); ?>" class="products_item products_item-2">
				<img src="/img/aes5.png" class="products_item-img">
				<span class="products_item-title">Комплектация<br> оборудованием</span>
				
				<div class="products_item-hover"></div>
			</a>
		</div> -->

		<div class="products_row">	
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'proektirovanie']); ?>" class="products_item">
				<img src="/img/aes1.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Проектирование</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'stroy-montaj']); ?>" class="products_item products_item__last">
				<img src="/img/aes3.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Строительно-монтажные,<br>пусконаладочные работы</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
		</div>
		<div class="products_row products_row__last">
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'garantiynoe-i-servisnoe']); ?>" class="products_item">
				<img src="/img/aes2.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Гарантийное и сервисное<br> обслуживание</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya']); ?>" class="products_item products_item__last">
				<img src="/img/aes5.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Комплектация<br> оборудованием</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
		</div>

		<!-- <div class="products_clm">
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'proektirovanie']); ?>" class="products_item">
				<img src="/img/aes1.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Проектирование</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'garantiynoe-i-servisnoe']); ?>" class="products_item">
				<img src="/img/aes2.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Гарантийное и сервисное<br> обслуживание</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
		</div>
		<div class="products_clm">
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'stroy-montaj']); ?>" class="products_item">
				<img src="/img/aes3.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Строительно-монтажные,<br>пусконаладочные работы</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
			<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya']); ?>" class="products_item">
				<img src="/img/aes5.png" class="products_item-img">
				<div class="products_item-text-wr">
					<div class="products_item-text">Комплектация<br> оборудованием</div>
				</div>
				<div class="prosducts_item-more">подробнее</div>
			</a>
		</div> -->
	</div>
</div>

<div class="activities">
	
	<div class="activities-line p-line">	
		<div class="label-line">
			<img class="label-line_img" src="/img/green-label.png">
		</div>
		<div class="activities_title">
			Наши преимущества
		</div>
		
		<div class="activities_items">
			<div class="activities_item-line">
				<div class="activities_item activities_item__1">
					Предоставляем <b>комплексный подход</b>
					<div class="activities_item-corner"></div>
				</div>
				<div class="activities_full-item activities_full-item__1">
					Комплекс (с лат. complexus – связь, сочетание) означает совокупность элементов, составляющих одно целое. Очевидно, такой сущностный признак «комплекса», как связь, сочетание, позволяют употреблять его в качестве синонима термину «система». Однако между системным и комплексным подходом имеются различия.
				</div>
			</div>
			<div class="activities_item-line">
				<div class="activities_item activities_item__2">
					Более <b>15-лет</b> успешной работы на рынке 
					<div class="activities_item-corner"></div>
				</div>
				<div class="activities_full-item activities_full-item__2">
					В идеале определиться, какая именно работа доставляет вам удовольствие, надо еще в юности. Впрочем, в жизни встречается немало примеров, когда специалист менял профессию в достаточно зрелом возрасте и добивался немалых успехов на новом поприще. 
					Выбрав специальность, уже в первые годы работы человек обычно понимает, что ему более интересно в рамках этого направления. К примеру, врач может принимать пациентов или заниматься административной работой в больнице. Журналист — писать статьи или организовывать работу репортерской службы. Менеджер — договариваться о конкретных сделках или определять общее направление развития компании. 
				</div>
			</div>
			<div class="activities_item-line">
				<div class="activities_item activities_item__3">
					Имеем <b>квалифицированный</b> персонал и <b>необходимые для работы документы</b>
					<div class="activities_item-corner"></div>
				</div>
				<div class="activities_full-item activities_full-item__3">
					специально подготовленные работники, прошедшие проверку знаний в объеме, обязательном для данной работы (должности), и имеющие группу по электробезопасности, предусмотренную действующими правилами охраны труда при эксплуатации электроустановок.
				</div>
			</div>
			<div class="activities_item-line">
				<div class="activities_item activities_item__4">
					<b>Выдерживаем</b> оговорённые <b>сроки</b> выполнения работ
					<div class="activities_item-corner"></div>
				</div>
			</div>
			<div class="activities_item-line">
				<div class="activities_item activities_item__5">
					Более <b>600</b> заказчиков: > 100 крупных, ~ 250 постоянных заказчиков 
					<div class="activities_item-corner"></div>
				</div>
			</div>
			<div class="activities_item-line">
				<div class="activities_item activities_item__6">
					Предоставляем гарантию на выполненные работы до <b>5 лет</b>
					<div class="activities_item-corner"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $this->render('@app/views/main/components/main-works',['portfolios'=>$_params_['portfolios']]); ?>

<div class="clients">
	<img class="clients_bg" src="/img/clients-bg.jpg">
	<div class="clients-line p-line">	
		<div class="label-line">
			<img class="label-line_img" src="/img/green-label.png">
		</div>
		<div class="title clients_title">
			Среди наших <span class="title_bold">клиентов</span>
		</div>
	
		<div class="clients_items">
			<div class="clients_item">
				<img src="/img/client-types/client-type1.png" class="clients_item-img">
				<div class="clients_item-title">Предприятия большой энергетики</div>
			</div>
			<div class="clients_item">
				<img src="/img/client-types/client-type2.png" class="clients_item-img">
				<div class="clients_item-title">Предприятия атомной энергетики</div>
			</div>
			<div class="clients_item">
				<img src="/img/client-types/client-type3.png" class="clients_item-img" style="padding-left:12px;">
				<div class="clients_item-title">Промышленные и перерабатывающие заводы</div>
			</div>
			<div class="clients_item">
				<img src="/img/client-types/client-type4.png" class="clients_item-img" style="padding-left:21px;">
				<div class="clients_item-title">Строительные компании</div>
			</div>
			<div class="clients_item">
				<img src="/img/client-types/client-type5.png" class="clients_item-img">
				<div class="clients_item-title">Проектные организации</div>
			</div>
			<div class="clients_item">
				<img src="/img/client-types/client-type6.png" class="clients_item-img">
				<div class="clients_item-title">Предприятия<br> ЖКХ</div>
			</div>
			<div class="clients_item">
				<img src="/img/client-types/client-type7.png" class="clients_item-img">
				<div class="clients_item-title">Дорожно-инфраструктурные орагнизации</div>
			</div>
		</div>
	</div>
</div>

<div class="c-logos">
	<div class="c-logos-line p-line">
		<div class="c-logos_dekor-wr">
			<div class="c-logos_dekor c-logos_dekor__l"></div>
			<div class="c-logos_dekor c-logos_dekor__r"></div>
		</div>	
		<div class="c-logos_items">
			<img src="/img/logos/chemk.jpg" class="c-logos_item">
			<img src="/img/logos/mechel.jpg" class="c-logos_item">
			<img src="/img/logos/chtpzz.jpg" class="c-logos_item">
			<img src="/img/logos/fortum.jpg" class="c-logos_item">
			<img src="/img/logos/mayak.jpg" class="c-logos_item">
			<img src="/img/logos/ogk2.jpg" class="c-logos_item">
			<img src="/img/logos/mmz.jpg" class="c-logos_item">
			<img src="/img/logos/vntf.jpg" class="c-logos_item">
			<img src="/img/logos/pz_trehg.jpg" class="c-logos_item">
			<img src="/img/logos/kmez.jpg" class="c-logos_item">
			<img src="/img/logos/makeev.jpg" class="c-logos_item">
			<img src="/img/logos/uyz.jpg" class="c-logos_item">
		</div>
		<!-- <div class="c-logos_btn-wr">
			<div href="" class="c-logos_showAll-btn">ВСЕ ЗАКАЗЧИКИ</div>
		</div> -->
	</div>
</div>


<div class="about" id="about">
	<img class="about_bg" src="/img/plusi-bg.png">
	<div class="about-line p-line">	
		<div class="label-line">
			<img class="label-line_img" src="/img/green-label.png">
		</div>
		<div class="title about_title">
			О <span class="title_bold">компании</span>
		</div>
		<p class="about_text">
			<strong>Агентство энергетического сервиса (АЭС)</strong> &ndash; компания, работающая на рынке автоматизированных систем управления технологическими процессами и учета энергоресурсов (воды, газа, электроэнергии, стоков и т.д.).
		</p>
		<p class="about_text">
			Мы располагаем большим штатом квалифицированных специалистов и можем выполнить весь комплекс работ для <strong>системы учета энергоресурсов</strong>, не прибегая к услугам сторонних организаций, от&nbsp;<strong>поставки измерительного и инженерного оборудования</strong> до <strong>организации узлов учета и систем регулирования &laquo;под ключ&raquo;</strong>, в том числе:
		</p>

		<!-- <ul class="about_list">
			<li class="about_list-item">Проектирование инженерных систем и сетей водоснабжения и водоотведения, теплоснабжения, КНС, ВНС.</li>
			<li class="about_list-item">Внедрение автоматизированных систем учета тепловой энергии, воды и стоков.</li>
			<li class="about_list-item">Ввод в эксплуатацию установленных приборов учета энергоресурсов.</li>
			<li class="about_list-item">Техническое обслуживание автоматизированных тепловых пунктов, блочно-модульных котельных, узлов учета энергоресурсов.</li>
			<li class="about_list-item">Организовываем систему сбора информации – диспетчеризацию.</li>
			<li class="about_list-item">Периодическую поверку и ремонт: расходомеров, преобразователей давления, преобразователей температуры, тепловычислителей / контроллеров / вычислителей.</li>
		</ul> -->
		<div class="about_clms">
			<div class="about_clm about_clm__1">
				<ul class="about_clm-ul">
					<li class="about_clm-li">
						Проектирование инженерных систем и сетей водоснабжения и водоотведения, теплоснабжения, КНС, ВНС.
					</li>
					<li class="about_clm-li">
						Внедрение автоматизированных систем учета тепловой энергии, воды и стоков.
					</li>
					<li class="about_clm-li">
						Ввод в эксплуатацию установленных приборов учета энергоресурсов.
					</li>				
				</ul>
			</div>
			<div class="about_clm about_clm__2">
				<ul class="about_clm-ul">
					<li class="about_clm-li">
						Техническое обслуживание автоматизированных тепловых пунктов, блочно-модульных котельных, узлов учета энергоресурсов.
					</li>
					<li class="about_clm-li">
						Организовываем систему сбора информации – диспетчеризацию.
					</li>
					<li class="about_clm-li">
						Периодическую поверку и ремонт: расходомеров, преобразователей давления, преобразователей температуры, тепловычислителей / контроллеров / вычислителей.
					</li>					
				</ul>
			</div>
		</div>
		<!--			
		<div class="about_clms">
			<div class="about_clm about_clm__1">
				<ul class="about_clm-ul">
					<li class="about_clm-li">
						Проектирование инженерных систем и сетей водоснабжения и водоотведения, теплоснабжения, КНС, ВНС
					</li>
					<li class="about_clm-li">
						Техническое обслуживание автоматизированных тепловых пунктов, блочно-модульных котельных, узлов учета энергоресурсов
					</li>
					<li class="about_clm-li">
						Ввод в эксплуатацию установленных приборов учета энергоресурсов
					</li>
					<li class="about_clm-li">
						Организовываем систему сбора информации – диспетчеризацию
					</li>
					<li class="about_clm-li">
						Первичную и периодическую поверку: расходомеров, преобразователей давления, преобразователей температуры, тепловычислителей / контроллеров / вычислителей 
					</li>
					<li class="about_clm-li">
						Ремонт измерительного, инженерного, насосного оборудования
					</li>
					<li class="about_clm-li">
						Гарантийное и сервисное обслуживание систем учета энергоресурсов и отдельных технических средств
					</li>
					<li class="about_clm-li">
						Внедрение автоматизированных систем учета тепловой энергии, воды и стоков
					</li>
					<li class="about_clm-li">
						Наладка систем автоматики блочно-модульных котельных
					</li>
				</ul>
			</div>
			<div class="about_clm about_clm__2">
				<ul class="about_clm-ul">
					<li class="about_clm-li">
						Монтаж и наладка первичных преобразователей расхода: Взлет (расходомеры ультразвуковые Влет МР, РСЛ (безнапорные трубопроводы, для открытых лотков), электромагнитные Взлет ЭМ, ЭР, ТЭР),  Сибнефтьавтоматика (расходомеры вихревые ДРГ на газ и пар), Vortex PhD (вихревые погружные), Yokogawa (расходомеры электромагнитные для агрессивных сред)
					</li>
					<li class="about_clm-li">
						Монтаж и наладка преобразователей температуры и давления
					</li>
					<li class="about_clm-li">
						Монтаж и наладка вторичных приборов Взлет, Логика;
					</li>
					<li class="about_clm-li">
						Установка и наладка программного обеспечения (Взлет СП, Энергосфера, ЛЭРС учет)
					</li>
					<li class="about_clm-li">
						Монтаж оборудование КИП и А
					</li>
					<li class="about_clm-li">
						Монтаж трубопроводной арматуры
					</li>
					<li class="about_clm-li">
						Установка оборудования пластинчатых тепло- обменников
					</li>
					<li class="about_clm-li">
						Монтаж насосного оборудования
					</li>
					<li class="about_clm-li">
						Монтажные и ремонтные работы на теплотрассах, трубопроводах, паропроводах.
					</li>
				</ul>
			</div>
		</div>
		-->
		<p class="about_text">
			Являемся аккредитованным сервисным центром <strong>ЗАО &laquo;Взлет&raquo;</strong>, <strong>ОАО ИПФ &laquo;Сибнефтеавтоматика&raquo;</strong>, <strong>ЗАО НПФ &laquo;Логика&raquo;</strong>, <strong>ЗАО &laquo;НПФ Теплоком&raquo;</strong>.
		</p>	
	</div>
</div>