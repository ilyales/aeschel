<?php
use yii\helpers\Url;
?>

<div class="text-page_title text-page_title__komplect">КОМПЛЕКТАЦИЯ ОБОРУДОВАНИЕМ</div>
<div class="text-page_text">
	<p>
		Большое значение для успешной реализации работ по внедрению систем учета энергоресурсов, а также их эффективной работы в течение максимально длительного периода имеет качество используемого оборудования, применяемого в составе данных систем. 
	</p>
	<div class="equipments-list">
		<div class="equipments-list_clm">
			<div class="equipments-list_clm-title">Измерительное</div>
			<div class="equipments-list_clm-items">
				<?php foreach ($this->params['allProductSubcats'][0] as $subcat): ?>
					<div class="equipments-list_clm-item">
						<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya','subCatagoryView' => 'izmeritelnoe-oborudivanie']); ?>#<?php echo $subcat["id"] ?>" class="equipments-list_clm-item-link"><?php echo $subcat["name"] ?></a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="equipments-list_clm">
			<div class="equipments-list_clm-title">Инженерное</div>
			<div class="equipments-list_clm-items">
				<?php foreach ($this->params['allProductSubcats'][1] as $subcat): ?>
					<div class="equipments-list_clm-item">
						<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya','subCatagoryView' => 'injenernoe-oborudivanie']); ?>#<?php echo $subcat["id"] ?>" class="equipments-list_clm-item-link"><?php echo $subcat["name"] ?></a>
					</div>
				<?php endforeach; ?>				
			</div>
		</div>
		<div class="equipments-list_clm">
			<div class="equipments-list_clm-title">Автоматизация</div>
			<div class="equipments-list_clm-items equipments-list_clm-items__last">
				<?php foreach ($this->params['allProductSubcats'][2] as $subcat): ?>
					<div class="equipments-list_clm-item">
						<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya','subCatagoryView' => 'oborudivanie-dly-avtomatizacii']); ?>#<?php echo $subcat["id"] ?>" class="equipments-list_clm-item-link"><?php echo $subcat["name"] ?></a>
					</div>
				<?php endforeach; ?>					
			</div>
		</div>
	</div>
</div>