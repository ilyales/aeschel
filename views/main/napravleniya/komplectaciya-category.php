<?php
 $subcats = $this->params['productSubcats'];
?>

<h3><?php echo $this->params['komplectaciya-category-title'] ?></h3>
<div class="equipments-detail">
	<div class="equipments-detail-cats">
		<?php foreach ($subcats as $index=>$subcat): ?>
			<?php if ($index==0) $activeClass = "equipments-detail-cats_item__active"; else $activeClass = "" ?>
			<a href="#<?php echo $subcat['id']?>" class="equipments-detail-cats_item <?php echo $activeClass ?>"><?php echo $subcat['name']?></a>
			<br>
		<?php endforeach; ?>
	</div>
	<div class="equipments-detail-items">
		
	</div>
</div>

<?php foreach ($subcats as $index=>$subcat): ?>
	<div equip-category="<?php echo $subcat['id']?>" class="equipments-detail-content" >
		<?php foreach ($subcat['manufactories'] as $manufactory): ?>
			<div class="equipments-detail-items_item">			
				<div class="equipments-detail-items_manuf">
					<img src="<?php echo $manufactory['img'] ?>" class="equipments-detail-items_manuf-logo">
				</div>
				<div class="equipments-detail-items_prods">
					<?php foreach ($manufactory['products'] as $product): ?>
						<span class="equipments-detail-items_prod" onclick="equipmentPopup.show('<?php echo $product['name'] ?>','<?php echo $product['img'] ?>')"><?php echo $product['name'] ?></span>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endforeach; ?>