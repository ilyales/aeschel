<?php
use yii\helpers\Url;
?>

<ul class="text-page-menu-list">
	<li class="text-page-menu-list_item <?php if ($activeItem=="proektirovanie") echo 'text-page-menu-list_item__active'?>">
		<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'proektirovanie']); ?>" class="text-page-menu-list_link">Проектирование</a>
	</li>
	<li class="text-page-menu-list_item <?php if ($activeItem=="garantiynoe-i-servisnoe") echo 'text-page-menu-list_item__active'?>">
		<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'garantiynoe-i-servisnoe']); ?>" class="text-page-menu-list_link">Гарантийное и сервисное обслуживание</a>
	</li>
	<li class="text-page-menu-list_item <?php if ($activeItem=="stroy-montaj") echo 'text-page-menu-list_item__active'?>">
		<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'stroy-montaj']); ?>" class="text-page-menu-list_link">Строительно-монтажные, пусконаладочные работы</a>
	</li>
	
	<li class="text-page-menu-list_item <?php if ($activeItem=="komplectaciya") echo 'text-page-menu-list_item__active'?>">
		<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya']); ?>" class="text-page-menu-list_link">Комплектация оборудованием</a>
		<ul class="text-page-menu-list_lev2-list <?php if ($activeItem=="komplectaciya") echo 'text-page-menu-list_lev2-list__visible'?>">
			<li class="text-page-menu-list_lev2-item <?php if ($activeSubItem=="izmeritelnoe-oborudivanie") echo 'text-page-menu-list_lev2-item__active'?>">
				<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya','subCatagoryView' => 'izmeritelnoe-oborudivanie']); ?>" class="text-page-menu-list_lev2-link">Измерительное</a>
			</li>
			<li class="text-page-menu-list_lev2-item <?php if ($activeSubItem=="injenernoe-oborudivanie") echo 'text-page-menu-list_lev2-item__active'?>">
				<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya','subCatagoryView' => 'injenernoe-oborudivanie']); ?>" class="text-page-menu-list_lev2-link">Инженерное</a>
			</li>
			<li class="text-page-menu-list_lev2-item <?php if ($activeSubItem=="oborudivanie-dly-avtomatizacii") echo 'text-page-menu-list_lev2-item__active'?>">
				<a href="<?php echo Url::toRoute(['napravleniya', 'catagoryView' => 'komplectaciya','subCatagoryView' => 'oborudivanie-dly-avtomatizacii']); ?>" class="text-page-menu-list_lev2-link">Автоматизация</a>
			</li>
		</ul>
	</li>			
</ul>
