<div class="works_item" item-num="<?php echo $_params_['num'] ?>" onclick="WorkViewer.show(this)">
    <div class="works_item-img-wrap">
        <img src="<?php echo $_params_['portfolio']['photo-mini']; ?>" class="works_item-img">
        <div class="work_item-hover">
            <div class="works_item-hover-btn">подробнее</div>
        </div>
    </div>          
    <div class="works_item-text">
        <?php echo $_params_['portfolio']['name'] ?>
    </div>
    <div class="works_item-dekor"></div>
    <div class="works_item-city">
        <?php echo $_params_['portfolio']['location'] ?>
    </div>

    <div class="works_item-full">
        <div class="work-viewer-content">
            <div class="work-viewer-content_head">  
                <div class="work-viewer-content_head-wrapForImg">   
                    <img src="<?php echo $_params_['portfolio']['photo-big']; ?>" class="work-viewer-content_head-img">
                </div>
                <div class="work-viewer-content_head-info"> 
                    <div class="work-viewer-content_head-title">
                        <?php echo $_params_['portfolio']['name'] ?>
                    </div>
                    <div class="work-viewer-content_head-city"><?php echo $_params_['portfolio']['location'] ?></div>
                </div>
            </div>
            <div class="work-viewer-content_body">
                <div class="work-viewer-content_body-text">
                    <?php echo $_params_['portfolio']['text'] ?>
                </div>
            </div>
        </div>
    </div>
</div>  