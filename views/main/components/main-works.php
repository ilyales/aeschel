<div class="works" id="works">
	<img class="works_bg1" src="/img/bg-soti-1.png">
 	<img class="works_bg2" src="/img/bg-soti-2.png">

	<div class="works-line p-line">	
		<div class="label-line">
			<img class="label-line_img" src="/img/green-label.png">
		</div>
		<div class="title works_title">
			Наши выполненые <span class="title_bold">проекты</span>
		</div>
	</div>
	<div class="works_text p-line ">
		Технологический учет теплоносителя на котельных, ЦТП, тепловых сетях, у абонетов. Технологический и коммерческий учет холодной воды на скважинах, насосных станциях, водоводах. Технологический учет газа среднего и низкого давления в ГРУ, ГРЩ и на газопроводах.
	</div>
	<div class="works_dekor-wr p-line ">
		<div class="works_dekor"></div>
	</div>	
	<div class="works_items p-line">
		<?php for ($i=0;$i<count($_params_['portfolios']);$i++):?>
			<?php 
				echo $this->render('@app/views/main/components/main-works-item',['portfolio'=>$_params_['portfolios'][$i],'num'=>$i+1]); 
			?>
		<?php endfor; ?>
	</div>
</div>

<div class="work-viewer-bg">
	<div class="work-viewer-bg_close"></div>
	<div class="work-viewer-bg_arrow-left"></div>
	<div class="work-viewer-bg_arrow-right"></div>
</div>
<div class="work-viewer"></div>