<div class="contacts">
	<div class="p-line">
		<div class="label-line p-line">
			<img class="label-line_img" src="/img/green-label.png">
		</div>
		<h1 class="contacts_title">Контакты</h1>
		<div class="contacts_map">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3fa571f9f07d5e9a34712480bf59ea4422cd18e6e897361990d72396a5a453cc&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
		</div>
		<div class="contacts_info">
			<div class="contacts_info-item">
				<div class="contacts_info-item-label">Почтовый адрес:</div>
				<div class="contacts_info-item-val">454038, г. Челябинск, ул. Талалихина, д. 17</div>
			</div>
			<div class="contacts_info-item">
				<div class="contacts_info-item-label">Телефон/факс:</div>
				<a href="tel:+73517313210" class="contacts_info-item-val">+7 (351) 731-32-10</a>
			</div>
			<div class="contacts_info-item">
				<div class="contacts_info-item-label">E-mail:</div>
				<a href="mailto:mail@aeschel.ru" class="contacts_info-item-val">mail@aeschel.ru</a>
			</div>
			<div class="contacts_info-item">
				<div class="contacts_info-item-label">Режим работы:</div>
				<div class="contacts_info-item-val">
					<div class="contacts_info-item-val-line"><b>понедельник - пятница:</b> 8.00 - 17.00</div>
					<div class="contacts_info-item-val-line"><b>суббота, воскресенье:</b> выходные</div>
				</div>
			</div>
		</div>
	</div>
</div>