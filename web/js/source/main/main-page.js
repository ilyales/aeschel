var targetHash = window.location.hash,
    targetHash = targetHash.replace('#', '');

// delete hash so the page won't scroll to it
window.location.hash = "";

// now whenever you are ready do whatever you want
// (in this case I use jQuery to scroll to the tag after the page has loaded)
$(window).load(function() {
    if (targetHash) {
        var scrollTop =  $("#" + targetHash).offset().top-90;
        $('html, body').animate({
            scrollTop: scrollTop,
        }, 500);
    }
});

$(window).on("load", function() {
    $('.c-logos_items').slick({
        slidesToShow: 7,
        slidesToScroll: 2,
        infinite: false,
        prevArrow:"<div class='c-logos_items-prev'></div>",
        nextArrow:"<div class='c-logos_items-next'></div>",
        autoplay:true,
        autoplaySpeed: 2000,
    });

    $('.header_menu-ul a[href*=\\#], .footer a[href*=\\#]').on('click', function(event){ 
        event.preventDefault();
        var href = $.attr(this, 'href').substring(1);
        var scrollTop = $(href).offset().top-90;
        $('html, body').animate({
            scrollTop: scrollTop,
        }, 500);
    });
}); 