var equipmentDetailContent = new EquipmentDetailContent;

routie({
  ':equip_category': function(equip_category) {
  	equipmentDetailContent.show(equip_category);
  }
});

function EquipmentDetailContent() {
	var equipmentsWrapEl = $('.equipments-detail-items');

	this.show = showItem;

	init();

	function init() {
		var contentWrapEl = $('.equipments-detail-content');
		if (contentWrapEl.length>0) {
			showItem(contentWrapEl.attr('equip-category'));
		}
	}

	function showItem(equip_category) {
		var contentWrapEl = $('.equipments-detail-content[equip-category="'+equip_category+'"]');
		if (contentWrapEl.length == 1) {
			equipmentsWrapEl.html(contentWrapEl.html());
			$('.equipments-detail-cats_item').removeClass('equipments-detail-cats_item__active');
			$('.equipments-detail-cats_item[href="#'+equip_category+'"]').addClass('equipments-detail-cats_item__active');
		}
	}
}