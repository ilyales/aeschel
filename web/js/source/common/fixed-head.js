var fixedHead = false;

$(window).scroll(function() {
    var pos = $(this).scrollTop();   

    if (pos>89) {
    	if (!fixedHead)
    	{
    		$('.header-wrap').addClass('header-wrap__fixed');
        	$('.main-slider').css('margin-top',89);
        	fixedHead = true;
    	}  	
    }
    else{
    	if (fixedHead)
    	{
	        $('.header-wrap').removeClass('header-wrap__fixed');
	        $('.main-slider').css('margin-top',0);
	        fixedHead = false;
	    }
    }  
});