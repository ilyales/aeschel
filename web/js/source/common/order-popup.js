$(document).ready(function() {
   window.orderPopupForm = new window.CustomForm({
    formSelector:'.order-popup',
    apiUrl:'/form/send',
    fieldsForValidation:{
      name:true,
      phone:true,
      email:true,
      confirm:true,
    }
  });
   window.orderPopup = new OrderPopup;
});

function OrderPopup() {
	var popup = $('.order-popup');
	var bg = $('.order-popup__bg');
	var prodNameEl = $('.order-popup__prod-name');
	var prodImgEl = $('.order-popup__prod-img');
	
	this.show = show;
	this.close = close;
	this.send = send;

	function show() {
		window.orderPopupForm.clear();
		var top = $(document).scrollTop() + 30;
		popup.css('top',top);
		popup.addClass('equip-popup_visible');
		bg.addClass('equip-popup__bg_visible');
		//$('body').css('overflow','hidden');
	}

	function close() {
		popup.removeClass('equip-popup_visible');
		bg.removeClass('equip-popup__bg_visible');
		$('body').css('overflow','visible');
	}

	function send() {
		close();
	}
}