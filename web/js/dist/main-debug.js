var targetHash = window.location.hash,
    targetHash = targetHash.replace('#', '');

// delete hash so the page won't scroll to it
window.location.hash = "";

// now whenever you are ready do whatever you want
// (in this case I use jQuery to scroll to the tag after the page has loaded)
$(window).load(function() {
    if (targetHash) {
        var scrollTop =  $("#" + targetHash).offset().top-90;
        $('html, body').animate({
            scrollTop: scrollTop,
        }, 500);
    }
});

$(window).on("load", function() {
    $('.c-logos_items').slick({
        slidesToShow: 7,
        slidesToScroll: 2,
        infinite: false,
        prevArrow:"<div class='c-logos_items-prev'></div>",
        nextArrow:"<div class='c-logos_items-next'></div>",
        autoplay:true,
        autoplaySpeed: 2000,
    });

    $('.header_menu-ul a[href*=\\#], .footer a[href*=\\#]').on('click', function(event){ 
        event.preventDefault();
        var href = $.attr(this, 'href').substring(1);
        var scrollTop = $(href).offset().top-90;
        $('html, body').animate({
            scrollTop: scrollTop,
        }, 500);
    });
}); 
function WorkViewer() {
    var viewerEl = $('.work-viewer');
    var viewerBgEl = $('.work-viewer-bg');
    var viwerCloseBtnEl = $('.work-viewer-bg_close');
    var nextBtnEl = $('.work-viewer-bg_arrow-right');
    var prevBtnEl = $('.work-viewer-bg_arrow-left');
    var currentItemIndex;
    var itemsCount;

    this.show = show;

    $(document).ready(init);

    function init() {
        viwerCloseBtnEl.on("click",close);
        nextBtnEl.on("click",next);
        prevBtnEl.on("click",prev);
        itemsCount = $('.works_item').length;   
        
        $('.works_items').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            //infinite: false,
            prevArrow:"<div class='works_items-prev'></div>",
            nextArrow:"<div class='works_items-next'></div>",
            autoplay:true,
        });
    }

    function show(itemNode) {
        var itemEl = $(itemNode);
        currentItemIndex = itemEl.attr('item-num');
        var viewerContent = itemEl.children('.works_item-full').html();
        viewerEl.html(viewerContent);
        viewerEl.show();
        viewerBgEl.show();
    }

    function next() {
        currentItemIndex++;
        if (currentItemIndex>itemsCount) {
            currentItemIndex = 1;
        }
        var itemEl = $(".works_items").find(".works_item[item-num='" + currentItemIndex + "']");
        var viewerContent = itemEl.children('.works_item-full').html();
        viewerEl.html(viewerContent);
    }

    function prev() {
        currentItemIndex--;
        if (currentItemIndex<1) {
            currentItemIndex = itemsCount;
        }
        var itemEl = $(".works_items").find(".works_item[item-num='" + currentItemIndex + "']");
        var viewerContent = itemEl.children('.works_item-full').html();
        console.log(itemEl);
        viewerEl.html(viewerContent);
    }

    function close() {
        viewerEl.hide();
        viewerBgEl.hide();
    }
}

$(document).ready(function() {
  var wv = new WorkViewer(); 
  window.WorkViewer = wv; 

});