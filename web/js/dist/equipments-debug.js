$(document).ready(function() {
   window.equipmentForm = new window.CustomForm({
    formSelector:'.equip-popup',
    apiUrl:'/form/send',
    fieldsForValidation:{
      name:true,
      phone:true,
      email:true,
      confirm:true,
    }
  });
   window.equipmentPopup = new EquipmentPopup;
});

function EquipmentPopup() {
	var popup = $('.equip-popup');
	var bg = $('.equip-popup__bg');
	var prodNameEl = $('.equip-popup__prod-name');
	var prodImgEl = $('.equip-popup__prod-img');
	
	this.show = show;
	this.close = close;
	this.send = send;

	function show(prodName,prodImgSrc) {
		window.equipmentForm.clear();
		var top = $(document).scrollTop() + 50;
		popup.css('top',top);
		prodNameEl.text(prodName);
		prodImgEl.attr('src', prodImgSrc);
		popup.addClass('equip-popup_visible');
		bg.addClass('equip-popup__bg_visible');
		//$('body').css('overflow','hidden');
	}

	function close() {
		popup.removeClass('equip-popup_visible');
		bg.removeClass('equip-popup__bg_visible');
		$('body').css('overflow','visible');
	}

	function send() {
		close();
	}
}
var equipmentDetailContent = new EquipmentDetailContent;

routie({
  ':equip_category': function(equip_category) {
  	equipmentDetailContent.show(equip_category);
  }
});

function EquipmentDetailContent() {
	var equipmentsWrapEl = $('.equipments-detail-items');

	this.show = showItem;

	init();

	function init() {
		var contentWrapEl = $('.equipments-detail-content');
		if (contentWrapEl.length>0) {
			showItem(contentWrapEl.attr('equip-category'));
		}
	}

	function showItem(equip_category) {
		var contentWrapEl = $('.equipments-detail-content[equip-category="'+equip_category+'"]');
		if (contentWrapEl.length == 1) {
			equipmentsWrapEl.html(contentWrapEl.html());
			$('.equipments-detail-cats_item').removeClass('equipments-detail-cats_item__active');
			$('.equipments-detail-cats_item[href="#'+equip_category+'"]').addClass('equipments-detail-cats_item__active');
		}
	}
}